package se.biffnet.pytteweb;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * This class represent one response, which includes the
 * status message, filepath and protocol. This information
 * will be assembled and returned with the getResponse() method. 
 * @author Andreas Andersson, anan8777
 *
 */
public class PytteHTTPResponse {

	private String	m_responseProtocol;
	private String 	m_responseMethod;
	private String 	m_responseFile;
	private String	m_responseStatus;
	
	private SimpleDateFormat m_dateFormat;
	
	public PytteHTTPResponse(PytteHTTPRequest request) {
		
		m_responseMethod = request.get_requestMethod();
		m_responseFile = request.get_requestFile();
		m_responseProtocol = "HTTP/1.1";
		
		// Setup the format for the date that is required for HTTP 
		m_dateFormat = new SimpleDateFormat("EEE, dd MMM y HH:mm:ss z", Locale.US);
		m_dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
	}
	
	/**
	 * Puts the headers together for this response. 
	 * @return String with the response.
	 */
	public String getResponse() {

		StringBuffer responseBuffer = new StringBuffer();
		
		responseBuffer.append(m_responseProtocol).append(" ");
		responseBuffer.append(m_responseStatus);
		responseBuffer.append("Date: ")
					.append(m_dateFormat.format(new Date()))
					.append("\r\n");
		
		File currentFile = new File(System.getProperty("user.dir") 
				+ m_responseFile);
	
		if(currentFile != null && currentFile.isFile() && currentFile.exists()) {
			
			try {
				String mimeType = Files.probeContentType(currentFile.toPath());
				responseBuffer.append("Content-Type: ").append(mimeType + "; \r\n");
			} catch (IOException e) {
				System.out.println("Could not determine mime type: " + e.getMessage());
				e.printStackTrace();
			}
			
			responseBuffer.append("Content-Length: ")
						.append(currentFile.length())
						.append("\r\n");
		}
		
		responseBuffer.append("\r\n");
		
		// For debug: System.out.println(responseBuffer.toString());
		
		return responseBuffer.toString();
		
	}
	
	/**
	 * Sets the status for this response.
	 * @param statusCode An integer with the code. 
	 */
	public void setStatusCode(int statusCode) {
		switch(statusCode) {
		case 200:
			m_responseStatus = String.valueOf(statusCode) + " OK\r\n"; break;
		case 400:
			m_responseStatus = String.valueOf(statusCode) + " Bad Request\r\n"; break;
		case 404:
			m_responseStatus = String.valueOf(statusCode) + " Not Found\r\n"; break;
		}
	}
	
	
	/**
	 * Getters and setters
	 */
	
	public void setResponseFile(String fileName) {
		m_responseFile = fileName;
	}
	
	public String getFilePath() {
		return m_responseFile;
	}

	public String getResponseMethod() {
		return m_responseMethod;
	}
	
	public void setResponseMethod(String method) {
		m_responseMethod = method;
	}

}