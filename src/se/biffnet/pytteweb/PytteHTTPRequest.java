package se.biffnet.pytteweb;

/**
 * Class that parses the request received by the 
 * server and stores the different data of the request. 
 * @author Andreas Andersson, anan8777
 *
 */
public class PytteHTTPRequest {
	
	private String m_requestRaw;
	private String m_requestMethod;
	private String m_requestFile;
	private String m_requestProtocol;
	private boolean m_validRequest;
	
	/**
	 * The constructor for the class which takes the whole HTTP
	 * request as a String as a parameter. 
	 * @param rawRequest The complete HTTP request. 
	 */
	public PytteHTTPRequest(String rawRequest) {
		// For debug: System.out.println(rawRequest);
		m_requestRaw = rawRequest;
		m_requestMethod = "";
		m_requestFile = "";
		m_requestProtocol = "";
		m_validRequest = false;
		parseRequest();
	}
	
	/**
	 * Extracts the method and filepath from the request. 
	 */
	private void parseRequest() {
		int index1,index2;
		m_validRequest = false;
		
		index1 = m_requestRaw.indexOf(' ');
		if (index1 != -1 && index1>0) {
			m_requestMethod = m_requestRaw.substring(0,index1); 
			index2 = m_requestRaw.indexOf(' ', index1 + 1);
			
			if (index2 > index1) {
				m_requestFile =  m_requestRaw.substring(index1 + 1, index2);
				index1 = m_requestRaw.indexOf('\r', index2 + 1);
				
				if(index1 > index2) {
					m_requestProtocol = m_requestRaw.substring(index2 + 1, index1);
					
					if(m_requestProtocol.equals("HTTP/1.1")) {
						m_validRequest = true;
					}
				}
			}
		}
	}
	
	
	/**
	 * Getters and setters.
	 */

	public String get_requestRaw() {
		return m_requestRaw;
	}

	public String get_requestMethod() {
		return m_requestMethod;
	}

	public String get_requestFile() {
		return m_requestFile;
	}
	
	public String get_requestProtocol() {
		return m_requestProtocol;
	}

	public boolean isValidRequest() {
		return m_validRequest;
	}
	
	public void set_requestFile(String requestFile) {
		m_requestFile = requestFile;
	}

}
