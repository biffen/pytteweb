package se.biffnet.pytteweb;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class PytteConsoleUI {
	
	private static final int DEFAULT_PORT = 8080;
	
	private static int m_listeningPort;
	private static PytteServer m_webServer;
	private static Thread m_thread;

	public static void main(String[] args) {
		
		m_listeningPort = DEFAULT_PORT; 

		if(args.length == 1) {
			if(!readParameterPort(args[0])) {
				readParameterFile(args[0]);
			}
		}
		
		//For debug: System.out.println("Port: " + String.valueOf(listeningPort));
		
		m_webServer = new PytteServer(m_listeningPort);
		m_thread = new Thread(m_webServer);
		m_thread.start();
		
		try {
			m_webServer.startServer();
		} catch (Exception e) {
			System.out.println("Could not open the ServerSocket.");
			e.printStackTrace();
		}

	}

	private static boolean readParameterPort(String firstParameter) {
		try {
			int newPort = Integer.parseInt(firstParameter);
			m_listeningPort = newPort;
			return true;
		} catch(NumberFormatException e) {
			System.out.println("Parameter was not a correct port number");
			return false;
		}
	}

	/**
	 * Should read port from file. Work in progress untill I know
	 * if this is where it should be implemented.
	 * @param firstParameter
	 */
	private static void readParameterFile(String firstParameter) {
		File parameterFile = new File(firstParameter);
		
		if(parameterFile.exists() && parameterFile.isFile()) {
		
			Path file = Paths.get(firstParameter);
			try {
				List<String> fileContent = Files.readAllLines(file, StandardCharsets.UTF_8);
				//Files.readAllBytes(file);
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			
		}
	}

}
