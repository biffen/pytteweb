
PytteWeb 1.0
Created by: Andreas Andersson, anan8777
Last edited: 2014-02-26

This web server software can handle basic HTTP 1.1 GET and HEAD requests and 
will return the necessary headers. If the file requested exists it will be 
returned, otherwise the server will return error.html. 

If the request is a HEAD-request only the headers will be returned. 

The easiest way to run the server is to start the class PytteConsoleUI
as a Java application. It is also possible to use the PytteGUI class 
but the application can no longer print status messages to the GUI 
and it is therefore not very useful.

You can use and start the server from any Java application with the following code:

		PytteServer pytteServer = new PytteServer(m_listeningPort);
		Thread thread = new Thread(pytteServer);
		thread.start();
		try {
			pytteServer.startServer();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
Use pytteServer.stopServer(); to take the server offline. 

-----------------------------------------------------------------------------

Om m�jligt, kan du f�rklara hur det �r t�nkt att man ska implementera f�ljande:
 - Portnummer kan tas in som parameter till main eller l�sas fr�n fil (om ingen 
 ges anv�nds port 8080 som default)
 
R�cker det att g�ra som jag g�r nu, att kunna ta in portnummer i klassens konstruktor?
Eller �r det meningen att klassen PytteServer ska ha en main-metod med argument, s� 
som PytteConsoleUI har nu?

Tack. 

-----------------------------------------------------------------------------