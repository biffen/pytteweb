package se.biffnet.pytteweb;

import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import java.awt.FlowLayout;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class takes care of showing the server interface and 
 * launches the separate thread for the actual server code.
 * @author Andreas Andersson, anan8777
 *
 */
public class PytteGUI {
	private Thread m_myThread;
	private PytteServer m_myWebServer;

	private JFrame m_frmPytteweb;
	private JTextField m_txtStopped;
	private JTextArea m_textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PytteGUI window = new PytteGUI();
					window.m_frmPytteweb.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PytteGUI() {
		initialize();
		addLogMessage("Thread started, server offline.");

		// Launches the actual server in a separate thread.
		// This enables the interface to work while the server is listening
		// for new connections. 
		m_myWebServer = new PytteServer();
		m_myThread = new Thread(m_myWebServer);
		m_myThread.start();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		// Makes the application look like a real windows application, 
		// not a ugly Java application.
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			
		}
		
		m_frmPytteweb = new JFrame();
		m_frmPytteweb.setTitle("PytteWeb 0.9");
		m_frmPytteweb.setBounds(100, 100, 673, 525);
		m_frmPytteweb.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m_frmPytteweb.getContentPane().setLayout(new BoxLayout(m_frmPytteweb.getContentPane(), BoxLayout.PAGE_AXIS));
		
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(20, 20, 20, 20));
		m_frmPytteweb.getContentPane().add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		panel_1.setPreferredSize(new Dimension(650, 35));
		panel_1.setMaximumSize(panel_1.getPreferredSize());
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					m_myWebServer.startServer();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				addLogMessage("Server online...");
				m_txtStopped.setText("Started");
			}
		});
		panel_1.add(btnStart);
		
		JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_myWebServer.stopServer();
				addLogMessage("Server offline...");
				m_txtStopped.setText("Stopped");
			}
		});
		panel_1.add(btnStop);
		
		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_3.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		panel_3.setPreferredSize(new Dimension(650, 35));
		panel_3.setMaximumSize(panel_1.getPreferredSize());
		panel.add(panel_3);
		
		JLabel lblStatus = new JLabel("Status");
		panel_3.add(lblStatus);
		
		m_txtStopped = new JTextField();
		m_txtStopped.setText("Stopped");
		m_txtStopped.setEditable(false);
		panel_3.add(m_txtStopped);
		m_txtStopped.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_2.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel.add(panel_2);
		
		m_textArea = new JTextArea();
		m_textArea.setColumns(75);
		m_textArea.setRows(20);
		m_textArea.setLineWrap(true);

		JScrollPane sp = new JScrollPane(m_textArea);
		
		panel_2.add(sp);
	}
	
	/**
	 * Adds a row to the log with timestamp. 
	 * @param message The message to be added. 
	 */
	public void addLogMessage(String message) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		m_textArea.setText(m_textArea.getText() 
				+ "\n"
				+ "[" + dateFormat.format(new Date()) + "] "
				+ message);
	}

}
