package se.biffnet.pytteweb;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This is the main class of the Pytte webserver. 
 * Pytte webserver is a simple web server that can serve
 * simple HTML files and headers according to HTTP 1.1. 
 * Got a lot of help from the following web site:
 * http://kcd.sytes.net/articles/simple_web_server.php
 * @author Andreas Andersson, anan8777
 */
public class PytteServer implements Runnable {
	
	// Buffer size when reading the file from disk.
	private static final int BUFFER_SIZE = 1024;
	
	// The port the server will listen for new connections on.
	private static final int DEFAULT_SERVER_PORT = 8080;
	
	// The file that will be returned when the requested file does not exist. 
	private static final String ERROR_FILENAME_404 = "/error404.html";
	private static final String ERROR_FILENAME_400 = "/error400.html";
	private static final String DEFAULT_FILENAME = "/index.html";
	
	// The currently used port.
	private int m_currentPort;
	
	private ServerSocket 			m_serverSocket;
	private static volatile boolean m_serverStatus;
	private StringBuffer 			m_requestBuffer;
	private String 					m_documentRoot;
	
	/**
	 * Default constructor that makes the server use the default port. 
	 */
	public PytteServer() {
		m_currentPort = DEFAULT_SERVER_PORT;
	}
	
	/**
	 * Constructor which takes in a port number to be used.
	 * @param portNumber The port number.  
	 */
	public PytteServer(int portNumber) {
		m_currentPort = portNumber;
	}
	
	/**
	 * The main method that will continuously listen for new connections. 
	 */
	@Override
	public void run() {

		m_documentRoot = System.getProperty("user.dir");
		
		// The infinity-loop that listens for connections. 
		while(true) {
			waitForServerStart();
			waitForConnections();
		}
	}
	
	/**
	 * Makes the application wait if its not started yet. 
	 */
	private void waitForServerStart() {
		try {
			synchronized(this) {
				while (getStatus() == false) {
				  	wait();
				}
			}
		} catch (InterruptedException e) {
			addLogMessage(e.getMessage());
		}
	}
	
	/**
	 * Waits for connections on the server-socket. 
	 */
	private void waitForConnections() {
		Socket socket = null;
		InputStream inputStream = null;
		OutputStream outputStream = null;
		
		try {
			socket = m_serverSocket.accept();
			if(getStatus() == true) {
				
				inputStream = socket.getInputStream();
				outputStream = socket.getOutputStream();
				String HTTPRequest = readInputStream(inputStream);
				
				// My browser sometimes sends a request that is empty. This is here
				// to ensure that we do not waste time on those. 
				if(HTTPRequest.length() < 5) {
					return;
				}
				
				PytteHTTPRequest pytteRequest = new PytteHTTPRequest(HTTPRequest);
				PytteHTTPResponse pytteResponse = new PytteHTTPResponse(pytteRequest);
				
				if(pytteRequest.isValidRequest()) { 
					validRequest(pytteRequest, pytteResponse);
				} else {
					invalidRequest(pytteResponse);
				}
				
				sendResponse(outputStream, pytteResponse);
			}
			socket.close();
			
		} catch (Exception e) {
			addLogMessage(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Handles a "valid" request and sets the status code and (if needed)
	 * the file to be returned in the response object. 
	 * @param pytteRequest The request.
	 * @param pytteResponse The response. 
	 */
	private void validRequest(PytteHTTPRequest pytteRequest, PytteHTTPResponse pytteResponse) {
		
		if (fileExist(pytteRequest.get_requestFile())) { 
			
			pytteResponse.setStatusCode(200);
			addLogMessage("Requested file '" + pytteRequest.get_requestFile() + "' found.");
			
		} else if(pytteRequest.get_requestFile().equals("/") && fileExist(DEFAULT_FILENAME)) {
			
			addLogMessage("No file specified, returning '" + DEFAULT_FILENAME + "'");
			pytteResponse.setResponseFile(DEFAULT_FILENAME);
			pytteResponse.setStatusCode(200);
			
		} else {
			
			addLogMessage("File '" + pytteRequest.get_requestFile() + "' does not exsists  (Error 404)");
			pytteResponse.setResponseFile(ERROR_FILENAME_404);
			pytteResponse.setStatusCode(404);
			
		}
	}
	
	/**
	 * Handles an invalid request. Sets the status code on the response to 400 and
	 * the file to be returned to the error file. 
	 * @param pytteRequest The request.
	 * @param pytteResponse The response. 
	 */
	private void invalidRequest(PytteHTTPResponse pytteResponse) {
		
		addLogMessage("Could not parse request (Error 400)");
		
		pytteResponse.setResponseFile(ERROR_FILENAME_400);
		pytteResponse.setStatusCode(400);
		
	}
	
	/**
	 * This method uses a OutputStream and a Response object to send headers
	 * and the file to the client that requested it. 
	 * @param outputStream The OutputStream that is used to send the file. 
	 * @param pytteResponse The response to send. 
	 * @throws IOException Throws exception if something goes wrong when
	 * closing the FileInputStream. 
	 */
	private void sendResponse(OutputStream outputStream, PytteHTTPResponse pytteResponse) {
		
		File file = new File(m_documentRoot + pytteResponse.getFilePath());
		try (FileInputStream fileInStream = new FileInputStream(file)) {
		
			// Sending headers
			byte[] responseBytes = pytteResponse.getResponse().getBytes();
			outputStream.write(responseBytes);
			
			// If this is a HEAD request, do not send content. 
			if(pytteResponse.getResponseMethod().equals("HEAD") 
					|| !file.isFile() 
					|| !file.exists()) {
				return;
			}
		
			// Sending file (if GET)
			byte[] byteBuffer = new byte[BUFFER_SIZE];
			int bytesRead = fileInStream.read(byteBuffer, 0, BUFFER_SIZE);
			while(bytesRead != -1) {
				outputStream.write(byteBuffer, 0, BUFFER_SIZE);
				bytesRead = fileInStream.read(byteBuffer, 0, BUFFER_SIZE);
			}
			
		} catch(IOException e) {
			addLogMessage("Catch in sending response: " + e.getMessage());
		}
		
	}

	/**
	 * This method checks if the incoming file path points
	 * to a existing file. 
	 * @param file The path of the file.
	 * @return True if the file exists, otherwise false. 
	 */
	private boolean fileExist(String file) {

		File currentFile = new File(m_documentRoot + file);
		if(currentFile.exists() && !currentFile.isDirectory()) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Method that reads data from a given InputStream and 
	 * returns the data as a String. 
	 * @param inputStream The InputStream to read from.
	 * @return A String with the data. 
	 */
	private String readInputStream(InputStream inputStream) {
		m_requestBuffer = new StringBuffer(2048);
		int i;
		byte[] buffer = new byte[2048];
		
		try {
			i = inputStream.read(buffer);
			for (int j=0; j<i; j++) {
				m_requestBuffer.append((char) buffer	[j]);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
			i = -1;
		}
		
		return m_requestBuffer.toString();
	}
	
	/**
	 * This method attempts to open a ServerSocket to listen for new connections 
	 * @param owner
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	public synchronized void startServer() throws UnknownHostException, IOException {
		addLogMessage("Trying to create listening socket on port " + String.valueOf(m_currentPort) + ".");
		m_serverSocket = new ServerSocket(m_currentPort, 1, InetAddress.getByName("127.0.0.1"));
		addLogMessage("Server is now listening on port " + String.valueOf(m_currentPort) + ".");
		m_serverStatus = true;
		serverNotify();
	}
	
	/**
	 * Method that close down the server, it should not listen for new connections. 
	 */
	public synchronized void stopServer() {
		m_serverStatus = false;
		serverNotify();
		addLogMessage("Server is now down and is not listening for new connections.");
	}
	
	/**
	 * Method that wakes the thread (breaks it out of the loop)
	 */
	private synchronized void serverNotify() { 
		notify(); 
	}
	
	private void addLogMessage(String message) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		System.out.println("[" 
				+ dateFormat.format(new Date()) + "] "
				+ message);
	}
	
	
	/**
	 * Getters and setters.
	 */
	
	public String getDocumentRoot() {
		return m_documentRoot;
	}

	private boolean getStatus() {
		return m_serverStatus;
	}

}
